import React from 'react';
import './App.scss';

import {DataConsumer} from './context/ContextProvider'

import {Header} from './components/Header/Header'
import {Navigation} from './components/Navigation/Navigation'

import {Switch, Route} from 'react-router-dom'
import * as Page from './pages'

const FullApp = () => {
  return(
    <>
      <Header />
      <div className="App">
        <Switch>
          <Route exact path='/' component={Page.Home}/>
          <Route path='/profile' component={Page.Profile} />
          <Route exact path='/events/:event' component={Page.Single_event} />
          <Route path='/participants' component={Page.Participants} />
          <Route path='/companies' component={Page.CompanyList} />
          <Route exact path='/company/:id' component={Page.CompanyDetail} />
          <Route path='/meetings' component={Page.Meeting} />
          <Route component={Page.Error_page} />
        </Switch>

      </div>
      <Navigation />
    </>
  )
}


const CompanyListOnly = () => {
  return(
    <DataConsumer>
    {
      value => {
        const { dataFetched, profileData} = value;

        if(dataFetched){
          return  (
            <>
              <Header fullName={profileData.fullName} user_id={profileData.id}/>
              <div className='App'>
                <Switch>
                  <Route exact path='/' component={Page.CompanyList}/>
                  <Route exact path='/company/:id' component={Page.CompanyDetail} />
                </Switch>
              </div>
            </>
          )   
        }else{
          return <h1>Loading</h1>
        }

      }
    }
  </DataConsumer>
  )
}


function App() {

  return (
    <CompanyListOnly />
  );
}

export default App;
