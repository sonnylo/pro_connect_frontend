import axios from 'axios';

export default {
    

    async authenticate() {
        const token = await this.getAccessToken();
        return this.validateIsPro(token);
    },


    async getAccessToken() {
        let token = null;
        const authUrl = 'https://api.iffr.com/oauth2/auth?client_id=proconnect&response_type=code&redirect_uri=http://localhost:3000/';
        // const authUrl = 'https://api.iffr.com/oauth2/auth?client_id=proconnect&response_type=code&redirect_uri=https://proconnect.iffr.com/';
        const url = new URL(window.location.href);
        const code = url.searchParams.get('code');
// console.log(code);
        if(code) {
            try {
                const data = {
                    "grant_type": "authorization_code",
                    "code": code,
                    "client_id": "proconnect"
                };
                const config = {headers: {'Content-Type': 'application/json'}};
                const response = await axios.post('https://api.iffr.com/oauth2/token', data, config);
// console.log(response);
                if(response && response.data && response.data.access_token) token = response.data.access_token;
            } catch (e) {
                if(e.response && e.response.status) {
                    window.location.href = authUrl;
                }
            }
        } else {
            window.location.href = authUrl;
        }
// console.log(token);
        return token;
    },


    async validateIsPro(token) {
        const data = {
            // "query": "query ProfileInfo { me { fullName badgeId photo isPro isCrew isPress isFestival isTigerMember isOrganisation isTigerMemberSupreme hasUnleashedMembership __typename }}"
            "query": "query ProfileInfo { me { id fullName isPro __typename  }}"
        };
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        };
        const response = await axios.post('https://api.iffr.com/graphql', data, config);


        return response && response.data && response.data.data && response.data.data.me && response.data.data.me.isPro ? response.data.data.me : false;
    },


}