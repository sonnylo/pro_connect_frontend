import React, { useState, useEffect } from 'react'
import socketIOClient from 'socket.io-client';
import axios from 'axios'

import auth from '../services/auth'

export const DataContext = React.createContext();

// socket.io endpoint to the entire back-end server
// const globalEnd = 'http://localhost:4000'
const globalEnd = 'https://desolate-falls-00779.herokuapp.com'

// connect to global endpoint
const io = socketIOClient(globalEnd, { transports: ['websocket']});

// const name = window.prompt("what is your chat name?")

const DataProvider = (props) => {

    // const APIurl = 'http://www.json-generator.com/api/json/get/bTXxfIDzVK?indent=2'
    const APIurl = 'http://localhost:4000/proconn/api/companies'
    const FavouriteAPIurl = 'http://localhost:4000/proconn/api/favourites'
    // const APIurl = 'https://desolate-falls-00779.herokuapp.com/proconn/api/companies'

    let [dataFetched, setDataFetched] = useState(false)
    let [companyData, setCompanyData] = useState(null)
    const [ favouriteList, setFavouriteList ] = useState([])

    const [profileData, setProfileData] = useState({
        id: '',
        fullName: ''
    })

    // Change this into fetching data from backend api url
    // const companies = Data.data.map(companie => companie)

    // function to be used to fetch the data
    const FetchingData = async(url) => {
        try{
            const response = await fetch(url,{mode: "cors"})
            const data = await response.json()
            if(!response.ok){
                throw Error(response.statusText)
            }
            return data
        }catch(err){
            console.log(err)
        }
    }

    const axiosFetch = (url) => {
        if(auth.authenticate()){
            
            auth.authenticate().then(res => 
                setProfileData({
                    id: res.id,
                    fullName: res.fullName
                })    
            )

            axios.get(url)
            .then(res => {
                setCompanyData(res.data)
                setDataFetched(true)
            })
            .catch(err => {
                console.log(err)
            })
            
        }
    }

    useEffect( () => {

        axiosFetch(APIurl)
        // FetchingData(APIurl)
        // .then(data => {
        //     if(!data){
        //         return setCompanyData([])
        //     }
        //     setDataFetched(true)
        //     return setCompanyData(data.data)
        // })
    },[])

    useEffect( () => {
        axios.get(FavouriteAPIurl).then(res => {
            setFavouriteList(res.data)
        }).catch(err => {
            console.log(err)
        })
        console.log(favouriteList)
    },[profileData])

    

    return(
        <DataContext.Provider value={{
            dataFetched,
            companyData,
            io,
            profileData
        }}>
            {props.children}
        </DataContext.Provider>
    )

}

const DataConsumer = DataContext.Consumer;

export {DataProvider, DataConsumer};