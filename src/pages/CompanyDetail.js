import React from 'react'
import { useLocation } from 'react-router-dom'

import Profile from '../components/Profile/Profile'
import IconLoader from '../components/Icons/IconLoader'

const CompanyDetail = () => {

    const {
        company, 
        companyProfile, 
        country,
        email,
        website,
        territories,
        mainWorkingSector,
        otherWorkingSector,
        focus,
        objectives,
        attendingGuests
    } = useLocation().state.companyValues

    const companyInformation = () => {
        return(
            <div className='company_info_page'>
                <div className="hero_image">
                </div>

                <div className="company_contacts_section">
                        <div className='company_contact'>
                            <div className='company_contact_top'>
                                <h2>{company}</h2>
                            </div>
                            <div className='company_contact_mid'>
                                {
                                    country !== undefined && <p><IconLoader title='map-pin' icon_className='map-pin-icon'/><span className='company_info_text'>{country}</span></p>
                                }
                                {
                                    email !== undefined && <p><IconLoader title='mail' icon_className='mail-icon'/><span className='company_info_text'>{email}</span></p>
                                }
                                {
                                    website !== undefined && <p><IconLoader title='globe' icon_className='globe-icon'/><span className='company_info_text'>{website}</span></p>
                                }
                            </div>
                            <div className='company_contact_tags'>
                                {
                                    territories !== undefined && <p><strong>Territories: </strong><span>{territories}</span></p>
                                }
                                {
                                    mainWorkingSector !== undefined && <p><strong>Main sector: </strong><span>{mainWorkingSector}</span></p>
                                }
                                {
                                    otherWorkingSector !== undefined && <p><strong>Other sector: </strong><span>{otherWorkingSector}</span></p>
                                }
                            </div>
                        </div>
                        <div className='favourite'>
                            <IconLoader title='heart' icon_className='favourite' />
                        </div>
                    </div>


                <div className='company_goals'>
                    {
                        (companyProfile !== undefined && companyProfile !== '' && companyProfile !== null) && <div className='company_profile'>
                            <h2>Profile</h2>
                            <span>{companyProfile}</span>
                        </div>
                    }
                    {
                        (objectives !== undefined && objectives !== '' && objectives !== null) && <div className='company_objectives'>
                            <h2>Objectives</h2>
                            <span>{objectives}</span>
                        </div>
                    }
                    {
                        (focus !== undefined && focus !== '' && focus !== null) && <div className='company_focus'>
                            <h2>Focus</h2>
                            <span>{focus}</span>
                        </div>
                    }
                </div>

                <div className='company_guests'>
                    {
                        attendingGuests.map((guest, index) => 
                            <Profile key={index}
                                url={null}
                                name={guest.name}
                                role={guest.role}
                                badges={guest.badges}
                            />
                        )
                    }
                </div>

            </div>
        )
    }

    return (
        <>
            {companyInformation()}
        </>
    )
}

export default CompanyDetail
