import React from 'react'

import {CompanyListMobile} from '../components/Company'

const CompanyList = () => {
    return (
        <div className='content companylist'>
            <CompanyListMobile />
        </div>
    )
}

export default CompanyList
