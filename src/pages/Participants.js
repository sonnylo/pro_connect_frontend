import React from 'react'

import EventCard from '../components/Cards/EventCard'

const Participants = () => {
    return (
        <div className='content participants'>
            <EventCard title='Reality Check' event_class='reality_check' />
            <EventCard title="Pro Hub" event_class='pro_hub' />
            <EventCard title="Rotterdam Lab" event_class='rotterdam_lab'/>
            <EventCard title="CineMart" event_class='cinemart'/>
        </div>
    )
}

export default Participants
