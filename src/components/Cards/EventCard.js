import React from 'react'
import Button from '../Button/Button'

const EventCard = ({title, event_class}) => {
    return (
        <div className={`event_card ${event_class}`} >
            <Button title={title} to={`/events/${title}`} />
        </div>
    )
}

export default EventCard
