import React from 'react'

import { Link } from 'react-router-dom'

const Button = ({title, to, children}) => {
    return (
        <div className="button">
            <Link to={to}>
                <h2>{title}</h2>
                {children}
            </Link>
        </div>
    )
}

export default Button
