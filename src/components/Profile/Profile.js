import React from 'react'
import IconLoader from '../Icons/IconLoader'

const Profile = ({url, name, role, badges}) => {
    return (
        <div className='profile_section'>
            {
                url != null ? <img src={url} className="profile-pic" alt="" /> : <IconLoader title='logo' icon_className='default_profile_pic' />
            }
            <p>{name}</p>
            {role && <p>{role || 'Not Availabel'}</p>}
            {badges && <span>{badges[0]}</span>}
        </div>
    )
}

export default Profile
