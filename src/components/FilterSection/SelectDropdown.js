import React from 'react'
import Select from 'react-select'

const SelectDropdown = ({label, onChange, options, name, multi}) => {
    return (
        <div className="select_dropdown">
            <p>{label}</p>
            <Select isMulti={multi} onChange={onChange} name={name} options={options} />
        </div>
    )
}

export default SelectDropdown
