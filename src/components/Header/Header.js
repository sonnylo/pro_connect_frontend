import React, { useContext} from 'react'
import IconLoader from '../Icons/IconLoader'

import axios from 'axios'

export const Header = ({fullName, user_id}) => {

    const user_ID = {
        id: user_id
    }

    const getData = (e) => {
        // axios.get('http://localhost:4000/proconn/api/favourites',
        //     {params: user_ID}
        // ).then(res => {
        //     console.log(res)
        // })
    }

    return (
        <div className="header">
            <div className="header_profile">
                <p onClick={getData}>{fullName}</p>
            </div>
            <div className="header_title">
                <IconLoader title="logo" icon_class="header_logo" />
                <h3>Pro Connect</h3>
            </div>
        </div>
    )
}
