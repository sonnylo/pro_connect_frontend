import React from 'react'

const ChatUser = ({activeUsers, roomName }) => {

    return (
        <div className='event_participants'>
            <div className='event_participants_total'>
                <h3>{roomName}</h3>
            </div>
            <div className='event_participants_list'>
                <ul className='event_participants_list_unordered'>
                    {activeUsers.map( (user,key)=> (
                        
                            <li key={key}>{user.username}</li>
                        )
                    )}
                </ul>
            </div>
        </div>
    )
}

export default ChatUser
