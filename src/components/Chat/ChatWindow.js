import React from 'react'

import IconLoader from '../Icons/IconLoader'
import ScrollToBottom from 'react-scroll-to-bottom'
 
const ChatWindow = ({chatLog, msg, onMsgChange, emptyMsg, sendChat}) => {

    return (
        <div className="chat_window">
            <ScrollToBottom className='chat_log'>
                {chatLog.map((chat, index) => 
                    
                    <div className={chat.isBot ? 'bot_chat' : 'user_chat'} key={index}>
                        <p>{chat.username}:</p>
                        <div className={chat.isBot ? 'bot_chat_msg' : 'user_chat_msg'}>
                            <p>{chat.text}</p>
                            <span className={`${chat.isBot ? 'bot_chat' : 'user_chat'} msg_time`}>
                                {chat.time}
                            </span>
                        </div>
                    </div>
                    
                )}
            </ScrollToBottom>
            <div className='chat_msg'>
                <input type='text' value={msg} placeholder="Type your message here" onChange={e => onMsgChange(e)}/>
                <button type='submit' onClick={e => sendChat(e)} disabled={emptyMsg}>
                    <IconLoader title='send' icon_className='send_button' />
                </button>
            </div>
        </div>
    )
}

export default ChatWindow

