import React, {useState, useContext, useEffect} from 'react'
import {DataContext} from '../../context/ContextProvider'
import axios from 'axios'

import IconLoader from '../Icons/IconLoader'

import SearchInput from '../FilterSection/SearchInput'
import List from './List';
import CompanyCard from './CompanyCard'

import Fuse from 'fuse.js'
import SelectDropdown from '../FilterSection/SelectDropdown'

import _ from 'lodash'

function CompanyListMobile() {

    const { dataFetched, companyData, profileData } = useContext(DataContext)
    const [ sortedList, setSortedList ] = useState([])
    const [ queryList, setQueryList ] = useState([])

    const [stateValues, setStateValues] = useState({
        country: [],
        mainWorkingSector: [],
        filterBadges: []
    })

    const [ filterOpen, setFilterOpen ] = useState(false)

    useEffect( () => {
        // only run this filter when the data is fetched and is not null
        if(dataFetched && companyData != null){
            setSortedList(companyData.sort((a,b)=>{
                if(a.company.toLowerCase().trim() > b.company.toLowerCase().trim()) return 1;
                if(a.company.toLowerCase().trim() < b.company.toLowerCase().trim()) return -1;
                return 0;
              }))
            
            // const countrylist = [...new Set(companyData.map( data => data.country).sort())].map(data => ({label: data, value: data}))
            // setCountries([{ label: 'All', value: "All"}, ...countrylist])
       
            // const sectorList = [...new Set(companyData.map(data=>data.mainWorkingSector).sort())].map(data => ({label: data, value: data}))
            // setSector([{ label: 'All', value:"All"}, ...sectorList])
            
            // const badgeList = [...new Set(companyData.map(badge => badge.filterBadges[0]).sort())].map(data=>({label: data, value: data}))
            // setBadges([{ label: 'All', value:"All"}, ...badgeList])
            
            // using lodash to make it easier
            const countrylist = _.uniq(_.map(companyData, (g) => g.country)).sort().map(data => ({label: data, value: data}));
            const sectorList = _.uniq(_.flatten(_.map(companyData, (g) => g.filterWorkingSectors))).sort().map(data => ({label: data, value: data}));
            const badgeList = _.uniq(_.map(companyData, g => g.filterBadges[0])).sort().map(data => ({label: data, value: data}));
            setStateValues({
                country: [{ label: 'All', value: "All"}, ...countrylist],
                mainWorkingSector: [{ label: 'All', value:"All"}, ...sectorList],
                filterBadges: [{ label: 'All', value: "All"}, ...badgeList]
            })
            
            setQueryList(sortedList)
        }
    },[companyData, dataFetched, sortedList])


    const fuse = new Fuse(sortedList, {
        shouldSort: true,
        threshold: 0.1,
        keys: [
            {
                name: 'company',
                weight: 0.1
            },
            {
                name: 'mainWorkingSector',
                weight: 0.1
            },
            {
                name: 'country',
                weight: 0.1
            },
            {
                name: 'filterGuest',
                weight: 0.1
            }
        ]
    })
    
    const handleInput = (e) => {
        const result = e ? fuse.search(e).map(result=>result.item) : sortedList
        setQueryList(result)
    }

    const onDropdownChange = (evt, meta) => {

        let newList = sortedList
        if(evt === null || evt[0].value === 'All'){
            setQueryList(sortedList)
        }else{
            evt.forEach( element => {
                newList = newList.filter(i => i[meta.name] !== undefined && i[meta.name].includes(element.value))
                setQueryList(newList)
            })
        }
    }

    const handleFilterOpen = () => {
        setFilterOpen(!filterOpen)
    }

    const handleFavouriteClick = (e) => {
        // console.log(`profilname: ${profileData.fullName} and companyID:${e}`)
        const data = {
            user_id: profileData.id,
            company_id: e
        }
        
        axios.post('http://localhost:4000/proconn/api/favourites', data, {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
              },
              params: data
        })
        .then(res => console.log(res))
    }

    // function to render a list of elements
    function renderList() {

        if(dataFetched){
            return(
                <>
                    <div className='filter_menu'>
                        <SearchInput onInput={handleInput} placeholder={'Type a name to search'}>
                            <span onClick={handleFilterOpen}>
                                {
                                    filterOpen ? <IconLoader title='close' icon_className='close_icon' /> : <IconLoader title='filter' icon_className='list_icon' />
                                }
                            </span>
     
                        </SearchInput>
                        <div className={`dropdown_filter ${filterOpen ? 'filter_open' : 'filter_close'}`}>
                            <SelectDropdown multi={true} label={'Select a country'} name="country" onChange={onDropdownChange} options={stateValues.country} />
                            <SelectDropdown multi={true} label={'Select a sector'} name="mainWorkingSector" onChange={onDropdownChange} options={stateValues.mainWorkingSector} />
                            <SelectDropdown multi={true} label={'Select a badge'} name="filterBadges" onChange={onDropdownChange} options={stateValues.filterBadges} />
                        </div>
                    </div>
                    {/* <List values={queryList} /> */}

                    <div className="card-lists">
                        {
                            queryList.map((company, index) => {
                                return (
                                    <CompanyCard
                                        key={index}
                                        companyValues={company}
                                        favourite={handleFavouriteClick}
                                    />
                                )
                            })
                        }
                    </div>
                </>
            )
        }else{
            return(
                <div className='loader'><IconLoader title='logo' icon_className='loader_company' /><h2>Loading</h2> </div>
            )
        }
    }

    
    return (
        renderList()
    )
}

export default CompanyListMobile
