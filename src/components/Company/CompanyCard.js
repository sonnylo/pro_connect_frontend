import React from 'react'
import {Link} from 'react-router-dom'

import IconLoader from '../Icons/IconLoader'

const CompanyCard = ({companyValues, favourite}) => {

    const {
        id,
        company,
        country,
        email,
        mainWorkingSector,
        attendingGuests
    } = companyValues

    const renderCard = () => {

        return(
            <div className='card-section' id={id}>

                <div className='top-section'>
                    <div className='top-section-text'>
                        <Link to={{
                            pathname:`/company/${id}`,
                            state: {companyValues}
                        }}>
                            <h2>{company}</h2>
                        </Link>
                        {(email !== null && email !== undefined) && <p>{email}</p>}
                    </div>
                    <div className='favourite-icon' onClick={(e)=> favourite(id)}>
                        <IconLoader title='heart' icon_className='favourite' />
                    </div>
                </div>
                <div className='section-tags'>
                    <p>
                        {(country !== null && country !== undefined) && <span>{country}</span>}
                        {
                            mainWorkingSector !== null && <span>{mainWorkingSector}</span>
                        }
                    </p>
                </div>
                <div className='card-guests'>
                    {attendingGuests.slice(0, 2).map((guest, id) => (
                        <div className='guest' key={id}>
                            {/* <img className="guest_pic" src={image} alt="" /> */}
                            <IconLoader title='logo' icon_className='guest-logo' /> <p>{guest.name}</p>
                        </div>
                    ) )}
                </div>
                
            </div>
        )
    }


    return (
        <>
            {renderCard()}
        </>
    )
}

export default CompanyCard